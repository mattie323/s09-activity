package com.zuitt.dicussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@SpringBootApplication
@RestController
//Will require all routes within the "Discussion Application" to use "/greeting" as part
@RequestMapping("/greeting")
//The "@RestController" annotation tells Spring Boot that this application will function as an endpoint that will be used in handling web requests
public class DicussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DicussionApplication.class, args);
	}

	@GetMapping("/hello")
	// Maps a get request to the route "/hello" and the method "hello()"
	public String hello(){
		return "Hello World";
	}
	// Routes with a string query
	// Dynamic data is obtained from the URL's string query
	// "%s" specifies that the value to be included in the format is of any data type
	//http://localhost:8080/hi?name=Joe
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "John") String name){
		return String.format("Hi %s",name);
		}
	@GetMapping("/friend")
	public String friend(@RequestParam(value="name",defaultValue = "Joe")String name, @RequestParam(value = "friend", defaultValue = "Jane") String friend){
		return String.format("Hello %s! My name is %s.",friend,name);
	}
	// Routes with path variables

	// localhost:8080/hello/joe
	// @PathVariable annotation allows us to extract data directly from the URL.
	@GetMapping("/hello/{name}")
	public String courses(@PathVariable("name") String name){
		return String.format("Nice to meet you %s!",name);
	}

	// ACTIVITY S09

	//Initialize a new ArrayList called enrollees in the DiscussionApplication class.
		ArrayList<String> enrollees = new ArrayList<String>();
	//	Create a /enroll route that will accept a query string with the parameter of user which:
	//	Adds the user’s name in the enrollees array list
	//	Returns a welcome (user)! Message.
		@GetMapping("/enroll")
		public String enrollUser(@RequestParam(value="user")String user){
			enrollees.add(user);
			return "Thank you for enrolling, "+user+"!";
		}
	//Create a new /getEnrollees route which will return the content of the enrollees ArrayList as a string.
		@GetMapping("/getEnrollees")
		public String getEnrollees(){
			return enrollees.toString();
		}
	//Create a /nameage route with that will accept multiple query string parameters of name and age and will return a Hello (name)! My age is (age). message.
		@GetMapping("/nameage")
		public String nameAge(@RequestParam(value="name")String name,@RequestParam(value="age") int age){
			return "Hello "+name+" My age is "+age;
		}
	//Create a /courses/id dynamic route using a path variable of id that:
		@GetMapping("/courses/{id}")
		public String getCourse(@PathVariable String id){
			 String courseName,schedule,price;
			 //If the path variable passed is “java101” return the course name, schedule and price.
			if(id.equals("java101")){
				courseName = "java101";
				schedule = "MWF 8:00 AM - 11:00 AM";
				price = "PHP 3000";
			}
			//If the path variable passed is “sql101” return the course name, schedule and price.
			else if(id.equals("sql101")){
				courseName = "sql101";
				schedule = "TTH 9:00 AM - 11:00 AM";
				price = "PHP 2500";
			}
			//If the path variable passed is “javaee101” return the course name, schedule and price.
			else if(id.equals("javaee101")){
				courseName = "javaee101";
				schedule = "MWF 1:30 PM - 4:00 PM";
				price = "PHP 4000";
			}
			//Else return a message that the course cannot be found.
			else{
				return "That course cannot be found";
			}
			return "Name: "+ courseName+", "+"Schedule: "+schedule+", "+"Price: "+price;
		}


}
